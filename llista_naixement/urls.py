from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path

from llista import views

urlpatterns = i18n_patterns(path("admin/", admin.site.urls), path("", views.index), prefix_default_language=False)
