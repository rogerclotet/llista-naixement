from django.contrib import messages
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

from llista.forms import ReserveForm
from llista.models import Product, Reserve


def index(request: HttpRequest) -> HttpResponse:
    products = Product.objects.all().order_by("price")
    reserved = [product for product in products if product.reserve_set.count() >= product.needed]
    missing = [product for product in products if product not in reserved]

    if request.method == "POST":
        reserve_form = ReserveForm(request.POST)
        if reserve_form.is_valid():
            if (
                Reserve.objects.filter(
                    product_id=reserve_form.cleaned_data.get("product"),
                    name__exact=reserve_form.cleaned_data.get("name"),
                ).count()
                > 0
            ):
                messages.error(request=request, message="Ja hi ha una reserva amb aquest nom per aquest producte")
            else:
                reserve_form.save()
                messages.success(request=request, message="Enviat correctament, moltes gràcies!")
        else:
            messages.error(
                request=request,
                message="Hi ha hagut algun problema. Torna a intentar-ho més tard o avisa'ns del problema, sisplau",
            )

    form = ReserveForm()

    return render(request, "index.html", {"missing": missing, "reserved": reserved, "form": form})
