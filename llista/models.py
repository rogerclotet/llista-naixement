from django.db import models
from django.utils.translation import gettext_lazy as _


class Product(models.Model):
    name = models.CharField(max_length=256, verbose_name=_("Nom"))
    description = models.TextField(blank=True, verbose_name=_("Descripció"))
    image_url = models.CharField(max_length=256, blank=True, verbose_name=_("Imatge"))
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, verbose_name=_("Preu"))
    url = models.CharField(max_length=256, verbose_name=_("Enllaç"))

    needed = models.IntegerField(default=1, verbose_name=_("En necessitem"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name = _("Producte")
        verbose_name_plural = _("Productes")


class Reserve(models.Model):
    name = models.CharField(max_length=256, verbose_name=_("Nom"))
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE, verbose_name=_("Producte"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Reserva")
        verbose_name_plural = _("Reserves")
