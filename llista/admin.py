from django.contrib import admin
from llista.models import Product, Reserve


class ReserveInline(admin.StackedInline):
    model = Reserve
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    inlines = (ReserveInline,)


admin.site.register(Product, ProductAdmin)
admin.site.register(Reserve)
