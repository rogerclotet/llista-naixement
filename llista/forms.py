from django import forms

from llista.models import Reserve


class ReserveForm(forms.ModelForm):
    class Meta:
        model = Reserve
        fields = ("name", "product")
        labels = {"name": "Nom i cognoms"}
        widgets = {"product": forms.HiddenInput()}
